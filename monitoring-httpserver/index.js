#!/usr/bin/node
const Discord = require('discord.js');
const express = require('express');
const app = express();

var pings = {};

function sendMessage(id, msg) {

    console.log("sendMessage: id=" + id + ", msg=" + msg );

    var ping = pings[id];

    ping.discordWebHookClient
        .sendMessage(msg)
        .then(function() {
            console.log("message sent");
        })
        .catch(function(err) {
            throw err;
        });
}

app.get('/ping', function (req, res) {

    var id = req.query.id;
    var timeout = req.query.timeout;
    var service = req.query.service;
    var discordtoken = req.query.discordtoken;
    var discordid = req.query.discordid;
    var message = req.query.message;

    if(!id) {
        console.log("missing id");
        return res.status(500).end("");
    }
    if(!timeout) {
        console.log("missing timeout");
        return res.status(500).end("");
    }
    if(!service) {
        console.log("missing service");
        return res.status(500).end("");
    }   
    if(!discordtoken) {
        console.log("missing discordtoken");
        return res.status(500).end("");
    }
    if(!discordid) { 
        console.log("missing discordid");
        return res.status(500).end("");
    }

    //new ping to register ?
    if(!pings[id]) {
        console.log("new ping register, service: " + service + ", id: " + id);
        pings[id] = {
            timeoutId: setTimeout(function() {
                sendMessage(id, message);
            }, timeout*1000),
            serviceName: service,
            discordWebHookClient: new Discord.WebhookClient(discordid, discordtoken)
        }
        res.status(200).send('OK');
        return;
    }

    //existing ping
    console.log("existing ping renewal, service: " + service + ", id: " + id);

    //clear existing timeout if remaining
    if(pings[id].timeoutId)
        clearTimeout(pings[id].timeoutId);
    
    //update timeOut
    pings[id].timeoutId = setTimeout(function() {
        sendMessage(id, message);
    }, timeout*1000);
    res.status(200).send('OK');   
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
